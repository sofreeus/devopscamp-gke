#!/bin/bash -x

# Build a GKE cluster
#  Make sure you're logged in to the correct account: `gcloud auth login`

if [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]]; then
 echo "usage: $0 <project_name> <region> <cluster_name>"
 exit 1
fi

set -eu
set -o pipefail

# Runtime parameters
NOW=$(date +%Y%m%d%H%M%S)
GCP_PROJECT=$1
CLUSTER_REGION=$2
CLUSTER_NAME=$3

# Hardcoded parameters you might want to change
GKE_VERSION="1.10.5-gke.4"
MACHINE_TYPE="n1-standard-1"
NODES_PER_ZONE="1"

# Launch the cluster with all our options
gcloud beta container --project "${GCP_PROJECT}" clusters create "${CLUSTER_NAME}" \
  --region "${CLUSTER_REGION}" \
  --username "admin" \
  --cluster-version ${GKE_VERSION} \
  --machine-type ${MACHINE_TYPE} \
  --image-type "COS" \
  --disk-type "pd-standard" \
  --disk-size "100" \
  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
  --num-nodes ${NODES_PER_ZONE} \
  --no-enable-cloud-logging \
  --no-enable-cloud-monitoring \
  --enable-ip-alias \
  --network "default" \
  --subnetwork "default" \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing \
  --no-enable-autoupgrade \
  --enable-autorepair

# Create a kubectl configuration for our new cluster
# This also sets the context to the new cluster
gcloud container clusters get-credentials ${CLUSTER_NAME} \
  --region ${CLUSTER_REGION} \
  --project ${GCP_PROJECT}

# Elevate our current account to full RBAC admin, which allows us to create roles
kubectl create clusterrolebinding cluster-admin-binding-${NOW} \
  --clusterrole cluster-admin \
  --user $(gcloud config get-value account)

# Create the supporting resources necessary for the Nginx ingress
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml

# Create the Nginx ingress service
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml

# Remove our RBAC admin access
kubectl delete clusterrolebinding cluster-admin-binding-${NOW}
