# DevOpsCamp GKE

This repository is intended to house the code we need to spin up GKE clusters.

---

#### build_gke_cluster.sh

Usage:

    ./build_gke_cluster.sh <project_name> <region> <cluster_name>
